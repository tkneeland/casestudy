-- Koronis
-- Galactica
-- Interstella
-- Astronomica
-- Heliosphere
-- Floral
-- Lunatic
-- Deuteronic
-- Eclipse
-- Jupiter

-- CREATE TABLE IF NOT EXISTS instruments (
--     inst_id INT AUTO_INCREMENT,
--     name varchar(30) NOT NULL UNIQUE,
--     PRIMARY KEY (inst_id)
-- );
#-------------------------------------------------

INSERT INTO instruments (name)
VALUES(
    'Astronomica'
    );

INSERT INTO instruments (name)
VALUES(
    'Borealis'
    );

INSERT INTO instruments (name)
VALUES(
    'Celestial'
    );

INSERT INTO instruments (name)
VALUES(
    'Deuteronic'
    );

INSERT INTO instruments (name)
VALUES(
    'Eclipse'
    );

INSERT INTO instruments (name)
VALUES(
    'Floral'
    );

INSERT INTO instruments (name)
VALUES(
    'Galactica'
    );

INSERT INTO instruments (name)
VALUES(
    'Heliosphere'
    );

INSERT INTO instruments (name)
VALUES(
    'Interstella'
    );

INSERT INTO instruments (name)
VALUES(
    'Jupiter'
    );

INSERT INTO instruments (name)
VALUES(
    'Koronis'
    );