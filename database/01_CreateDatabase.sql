DROP DATABASE TradeHistory;
CREATE DATABASE IF NOT EXISTS TradeHistory;
USE TradeHistory;

CREATE TABLE IF NOT EXISTS counterparties (
    cp_id INT AUTO_INCREMENT,
    name VARCHAR(20) NOT NULL UNIQUE,
    PRIMARY KEY (cp_id)
) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS users (
    user_id INT AUTO_INCREMENT,
    name VARCHAR(20) NOT NULL UNIQUE,
    password VARCHAR(80) NOT NULL,
    PRIMARY KEY (user_id)
) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS instruments (
    inst_id INT AUTO_INCREMENT,
    name VARCHAR(20) NOT NULL UNIQUE,
    PRIMARY KEY (inst_id)
) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS deals (
    deal_id INT NOT NULL UNIQUE,
    cp_id INT NOT NULL,
    inst_id INT NOT NULL,
    order_type VARCHAR(1), # B, S  (buy/sell)
    quantity INT,
    price DECIMAL(20,12) NOT NULL,
    timestamp DATETIME(6) NOT NULL,
    PRIMARY KEY (deal_id),
    FOREIGN KEY (cp_id) REFERENCES counterparties(cp_id) ON DELETE CASCADE,
    FOREIGN KEY (inst_id) REFERENCES instruments(inst_id) ON DELETE CASCADE
) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS order_types (
    order_type_id INT AUTO_INCREMENT,
    order_type VARCHAR(1), # B, S  (buy/sell)
    PRIMARY KEY(order_type_id)
) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;



-- Should ON DELETE CASCADE BE ALLOWED?
