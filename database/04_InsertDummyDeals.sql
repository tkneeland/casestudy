-- CREATE TABLE IF NOT EXISTS deals (
--     deal_id INT AUTO_INCREMENT,
--     cp_id INT NOT NULL,
--     inst_id INT NOT NULL,
--     orderType VARCHAR(1), # B, S  (buy/sell)
--     quantity INT,
--     price DECIMAL(20,10) NOT NULL,
--     timestamp TIMESTAMP NOT NULL,
--     PRIMARY KEY (deal_id),
--     FOREIGN KEY (inst_id) REFERENCES deal_types(dt_id) ON DELETE CASCADE
-- );

-- DATETIME has 6 digits precision microseconds
-- datetime format needs to be parsed before passing into database
-- Solution: Use String and convert in Python?

INSERT INTO deals (deal_id, cp_id,inst_id,order_type,quantity,price,timestamp)
VALUES(
    "20001",
    "3",
    "7",
    "B", 
    "835",
    "8352.517662524997",
    "2019-08-08 16:21:41.253907"
);

INSERT INTO deals (deal_id,cp_id,inst_id,order_type,quantity,price,timestamp)
VALUES(
    "20002",
    "1",
    "7",
    "S", 
    "453",
    "8321.517662123",
    "2019-08-08 16:22:42.253907"
);

INSERT INTO deals (deal_id,cp_id,inst_id,order_type,quantity,price,timestamp)
VALUES(
    "20003",
    "2",
    "6",
    "S", 
    "130",
    "83.51766219823",
    "2019-08-08 16:24:45.253907"
);

INSERT INTO deals (deal_id,cp_id,inst_id,order_type,quantity,price,timestamp)
VALUES(
    "20004",
    "4",
    "6",
    "B", 
    "123",
    "82.717662123",
    "2019-08-08 16:25:45.253987"
);

INSERT INTO deals (deal_id,cp_id,inst_id,order_type,quantity,price,timestamp)
VALUES(
    "20005",
    "5",
    "6",
    "B", 
    "130",
    "1231.51766219823",
    "2019-08-08 16:26:23.253917"
);

INSERT INTO deals (deal_id,cp_id,inst_id,order_type,quantity,price,timestamp)
VALUES(
    "20006",
    "3",
    "6",
    "S", 
    "123",
    "1243.717572121",
    "2019-08-08 16:26:45.253987"
);
