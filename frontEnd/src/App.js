import React from 'react';
import LoginForm from "./components/loginForm";
import './App.css';

function App() {
  return (
    <React.Fragment>
      <main className="container">
        <LoginForm />
      </main>
    </React.Fragment>
  );
}

export default App;
