import unittest
import json
import pandas as pd
from datetime import datetime
from numpy.testing import assert_allclose
from businessAnalytics import *

# test data for queries


class TestSum(unittest.TestCase):

    # dummy data
    # load json text as string
    # produces dictionary which can be accessed by key: value pairs

    def test_getAveragePrice(self):
        # selecting inst_id 6 (Floral), get average price for buys and sells
         #ARRANGE
        # sample data from database for testing
        dictData=[{"deal_id": "3", "cp_id":"2","inst_id":"6","order_type":"S","quantity":"130","price":"83.517662198230","timestamp":"2019-08-08 16:24:45.253907"},
        {"deal_id": "4", "cp_id":"4","inst_id":"6","order_type":"B","quantity":"123","price":"82.717662123000","timestamp":"2019-08-08 16:25:45.253987"}]
        # actual calculation result:
        expectedValue = 83.128729355332
        # ACT
        # calculate average price with our Python analytics
        averagePrice=getAveragePrice(dictData)
        # ASSERT/TEST
        assert_allclose(averagePrice,expectedValue,atol=.00000000001,rtol=0)

    def test_getTradeValue(self):
        # Get total value of single trade
        a=json.loads('{"instrumentName": "Deuteronic", "price": 8362.315821049884, "type": "S", "quantity": 4, "cpty": "Lewis", "time": "2019-08-12 22:52:24.255723"}')
        tradeTotal= a["quantity"]* a["price"]
        price = 33449.263284199536
        self.assertEqual(tradeTotal, price)

    def test_getEndingPosition(self):
        # selecting cp_id 5 (Nidia) and inst_id 6, get ending position
        #ARRANGE
        dictData=[{"deal_id": "5", "cp_id":"5","inst_id":"6","order_type":"B","quantity":"130","price":"1231.517662198230","timestamp":"2019-08-08 16:26:23.253917"},
        {"deal_id": "6", "cp_id":"5","inst_id":"6","order_type":"S","quantity":"123","price":"1243.717572121000","timestamp":"2019-08-08 16:26:45.253987"}]
        expectedPosition = 7
        result = getPosition(dictData)
        self.assertEqual(expectedPosition, result)

    def test_getEndProfit(self):
        # selecting cp_id 5 (Nidia) and inst_id 6, get ending position
        #ARRANGE
        dictData=[{"deal_id": "5", "cp_id":"5","inst_id":"6","order_type":"B","quantity":"130","price":"1231.517662198230","timestamp":"2019-08-08 16:26:23.253917"},
        {"deal_id": "4", "cp_id":"5","inst_id":"6","order_type":"B","quantity":"100","price":"1230.839320485736","timestamp":"2019-08-08 16:26:51.27817"},
        {"deal_id": "6", "cp_id":"5","inst_id":"6","order_type":"S","quantity":"123","price":"1243.721572121000","timestamp":"2019-08-08 16:27:45.253987"}, 
        {"deal_id": "7", "cp_id":"5","inst_id":"6","order_type":"S","quantity":"100","price":"1233.716836761987","timestamp":"2019-08-08 16:28:48.383847"}]
        expectedEndProfit = 1786.76802987041
        result = getEndProfit(dictData)
        getLastDate(dictData)
        # '%Y %d %m %I %M %S %f'
        assert_allclose(expectedEndProfit, result, atol=.0000000001,rtol=0)

    def test_getUnrealizedProfit(self):
        # selecting cp_id 5 (Nidia) and inst_id 6, get ending position
        #ARRANGE
        dictData=[{"deal_id": "5", "cp_id":"5","inst_id":"6","order_type":"B","quantity":"130","price":"1231.517662198230","timestamp":"2019-08-08 16:26:23.253917"},
        {"deal_id": "4", "cp_id":"5","inst_id":"6","order_type":"B","quantity":"100","price":"1230.839320485736","timestamp":"2019-08-08 16:26:51.27817"},
        {"deal_id": "6", "cp_id":"5","inst_id":"6","order_type":"S","quantity":"123","price":"1243.721572121000","timestamp":"2019-08-08 16:27:45.253987"}, 
        {"deal_id": "7", "cp_id":"5","inst_id":"6","order_type":"S","quantity":"100","price":"1233.716836761987","timestamp":"2019-08-08 16:28:48.383847"}]
        expectedUnrealizedProfit= 17.458740201715
        result = getUnrealizedProfit(dictData)
        assert_allclose(expectedUnrealizedProfit, result, atol=.0000000001,rtol=0)


    def test_Login(self):
        # test data from JSON object input for retrieval
        login = json.loads('{"name":"admin","password":"password"}')
        assert login["name"] == "admin", "Should return login name"

if __name__ == '__main__':
    unittest.main()

