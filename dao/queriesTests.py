import unittest
import json

# test data for queries


class TestSum(unittest.TestCase):

    # dummy data
    # load json text as string
    # produces dictionary which can be accessed by key: value pairs
    # a=json.loads('{"instrumentName": "Deuteronic", "price": 8362.315821049884, "type": "S", "quantity": 4, "cpty": "Lewis", "time": "2019-08-12 22:52:24.255723"}')
    b=json.loads('{"instrumentName": "Deuteronic", "price": 8410.983635409282, "type": "B", "quantity": 9, "cpty": "Lewis", "time": "2019-08-12 22:52:24.363409"}')
    c=json.loads('{"instrumentName": "Borealis", "price": 5759.487861524209, "type": "S", "quantity": 8, "cpty": "Nidia", "time": "2019-08-12 22:52:24.392435"}')
    d=json.loads('{"instrumentName": "Lunatic", "price": 1797.1933161225295, "type": "S", "quantity": 11, "cpty": "John", "time": "2019-08-12 22:52:24.543230"}')
    e=json.loads('{"instrumentName": "Lunatic", "price": 1774.1665019632785, "type": "S", "quantity": 256, "cpty": "Selvyn", "time": "2019-08-12 22:52:24.807562"}')
    f=json.loads('{"instrumentName": "Jupiter", "price": 3520.1228783966226, "type": "S", "quantity": 185, "cpty": "Lina", "time": "2019-08-12 22:52:24.859986"}')
    g=json.loads('{"instrumentName": "Borealis", "price": 5706.613790569843, "type": "S", "quantity": 456, "cpty": "Richard", "time": "2019-08-12 22:52:25.156376"}')
    h=json.loads('{"instrumentName": "Eclipse", "price": 9951.058326480697, "type": "S", "quantity": 2, "cpty": "Lina", "time": "2019-08-12 22:52:25.342082"}')
    i=json.loads('{"instrumentName": "Lunatic", "price": 1749.831863522759, "type": "S", "quantity": 199, "cpty": "Lina", "time": "2019-08-12 22:52:25.465613"}')
    j=json.loads('{"instrumentName": "Eclipse", "price": 10027.013160432698, "type": "B", "quantity": 27, "cpty": "Nidia", "time": "2019-08-12 22:52:25.704808"}')
    k=json.loads('{"instrumentName": "Jupiter", "price": 3498.2696810259454, "type": "S", "quantity": 49, "cpty": "John", "time": "2019-08-12 22:52:25.743678"}')
    l=json.loads('{"instrumentName": "Galactica", "price": 9861.179736587674, "type": "S", "quantity": 133, "cpty": "Lina", "time": "2019-08-12 22:52:25.863868"}')

    # a1=json.loads(a)
    # print(a["quantity"]*a["price"])

    def test_getTradeValue(self):
        a=json.loads('{"instrumentName": "Deuteronic", "price": 8362.315821049884, "type": "S", "quantity": 4, "cpty": "Lewis", "time": "2019-08-12 22:52:24.255723"}')
        tradeTotal= a["quantity"]* a["price"]
        price = 33449.263284199536
        print(tradeTotal)
        self.assertEqual(tradeTotal, price)

    def test_getTradeValue2(self):
        a=json.loads('{"instrumentName": "Deuteronic", "price": 8362.315821049884, "type": "S", "quantity": 4, "cpty": "Lewis", "time": "2019-08-12 22:52:24.255723"}')
        tradeTotal= a["quantity"]* a["price"]
        price = 33449.263284199536
        # print(tradeTotal)
        self.assertEqual(tradeTotal, price)

    def test_getTrade(self):
        trade= 1
        p=1
        print(trade)
        # self.assertEqual(var1, var2, msg=None)
        self.assertEqual(trade, p)

    def test_upper(self):
        self.assertEqual('foo'.upper(), 'FOO')

    
    def test_Login(self):
        login = json.loads('{"name":"admin","password":"password"}')
        assert login["name"] == "admi", "Should return login name"

if __name__ == '__main__':
    unittest.main()
    testSum=TestSum()
    testSum.test_getTradeValue()
    testSum.test_getTrade()
    testSum.test_Login