import datetime
import numpy
import pandas as pd

jsondata = {}


def getAveragePrice(dataList):
    total = 0
    quantityTotal = 0
    for dict in dataList:
        total += int(dict["quantity"]) * float(dict["price"])
        quantityTotal += int(dict["quantity"])
    averagePrice = total / quantityTotal
    return averagePrice

def getPosition(dataList):
    total = 0
    for dict in dataList:
        if (dict["order_type"]) == "B":
            total += int(dict["quantity"])
        else:
            total -= int(dict["quantity"])
    return total

def getEndProfit(dataList):
    totalBuy = 0
    totalBuyQuantity = 0
    totalSell = 0
    totalSellQuantity = 0
    averageBuyPrice = 0
    averageSellPrice = 0
    for dict in dataList:
        if (dict["order_type"]) == "B":
            totalBuy += int(dict["quantity"]) * float(dict["price"])
            totalBuyQuantity += int(dict["quantity"])
            averageBuyPrice = totalBuy / totalBuyQuantity
        else:
            totalSell += int(dict["quantity"]) * float(dict["price"])
            totalSellQuantity += int(dict["quantity"])
            averageSellPrice = totalSell / totalSellQuantity
    return min(totalBuyQuantity, totalSellQuantity) * (averageSellPrice - averageBuyPrice)

def getLastDate(datalist):
    date = pd.to_datetime("1970-08-08 16:26:51.27817")
    for dict in datalist:
        date1 = pd.to_datetime(dict["timestamp"])
        if date1 > date:
            date = date1
    return date

def getUnrealizedProfit(datalist):
    averageBuyPrice = 0
    averageSellPrice = 0
    totalBuy = 0
    totalSell = 0
    totalBuyQuantity = 0
    totalSellQuantity = 0
    currentPosition = getPosition(datalist)
    lastDate = getLastDate(datalist)
    lastPrice = 0
    unrealizedProfit = 0
    for dict in datalist:
        if (dict["order_type"]) == "B":
            totalBuy += int(dict["quantity"]) * float(dict["price"])
            totalBuyQuantity += int(dict["quantity"])
            averageBuyPrice = totalBuy / totalBuyQuantity
        else:
            totalSell += int(dict["quantity"]) * float(dict["price"])
            totalSellQuantity += int(dict["quantity"])
            averageSellPrice = totalSell / totalSellQuantity
    for dict in datalist:
        if pd.to_datetime(dict["timestamp"]) == lastDate:
            lastPrice = dict["price"]
            # current position is positive
    if currentPosition > 0:
        unrealizedProfit = currentPosition * (float(lastPrice) - averageBuyPrice)
    else:
        unrealizedProfit = currentPosition * (averageSellPrice - float(lastPrice))

    return unrealizedProfit
