

from RandomDealData import *
# from casestudy.dataGen.RandomDealData import *

def main():
    # Data Generation  
    generator = RandomDealData()
    instrumentList = generator.createInstrumentList()
    for i in range(2000):
        data = generator.createRandomData(instrumentList)
        print(data)

if __name__ == "__main__":
    main()