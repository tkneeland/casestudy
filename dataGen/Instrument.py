import numpy, random

class Instrument:
    __name = ""
    __price = 0.0
    __drift = 0.0
    __variance = 0.0
    __basePrice = 0.0
    

    def __init__(self, name, price=0.0, drift=0.0, variance=0.0):
        self.__name = name
        self.__price = price
        self.__drift = drift
        self.__variance = variance
        self.__basePrice = price

    # add other analysis functions as needed
    def getName(self):
        return self.__name

    def getPrice(self):
        return self.__price

    def getDrift(self):
        return self.__drift

    def getVariance(self):
        return self.__variance

    def calculateNextPrice(self, direction):
        newPriceStarter = self.__price + numpy.random.normal(0,1)*self.__variance + self.__drift
        newPrice = newPriceStarter if (newPriceStarter > 0) else 0.0
        if self.__price < self.__basePrice*0.4:
            self.__drift =( -0.7 * self.__drift )
        self.__price = newPrice*1.01 if direction=='B' else newPrice*0.99
        return self.__price